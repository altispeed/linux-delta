---------------------------------------------------------------------------
-- Copyright (C) 2019 KhronoSync Digital Solutions LLC
--
-- This program is free software: you can redistribute it and/or modify
-- it under the terms of the GNU General Public License as published by
-- the Free Software Foundation, either version 3 of the License, or
-- any later version.
--
-- This program is distributed in the hope that it will be useful,
-- but WITHOUT ANY WARRANTY; without even the implied warranty of
-- MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
-- GNU General Public License for more details.
--
-- You should have received a copy of the GNU General Public License
-- along with this program.  If not, see <https://www.gnu.org/licenses/>
---------------------------------------------------------------------------

UPDATE distros SET
  description = 'Pop!_OS is designed to inspire users to ' ||
    'learn, imagine the impossible, and then create it. ' ||
    'Increase your productivity on startup with features that ' ||
    'prioritize smooth workflow and hardware performance, as ' ||
    'well as easy access to developer toolkits like CUDA and ' ||
    'Tensorflow. Pop!_OS is meticulously engineered to provide ' ||
    'a polished, secure experience out of the box. Try it today!',
  description_source = 'https://s76.co/trypopnow'
WHERE name = 'Pop!_OS'