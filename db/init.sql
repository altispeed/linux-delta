---------------------------------------------------------------------------
-- Copyright (C) 2019 KhronoSync Digital Solutions LLC
--
-- This program is free software: you can redistribute it and/or modify
-- it under the terms of the GNU General Public License as published by
-- the Free Software Foundation, either version 3 of the License, or
-- any later version.
--
-- This program is distributed in the hope that it will be useful,
-- but WITHOUT ANY WARRANTY; without even the implied warranty of
-- MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
-- GNU General Public License for more details.
--
-- You should have received a copy of the GNU General Public License
-- along with this program.  If not, see <https://www.gnu.org/licenses/>
---------------------------------------------------------------------------

--------------
-- DISTROS
--------------
CREATE TABLE IF NOT EXISTS distros (
  id UUID PRIMARY KEY,
  name VARCHAR(100) UNIQUE,
  description TEXT NOT NULL,
  description_source VARCHAR(400),
  homepage VARCHAR(200) NOT NULL,
  tags VARCHAR(400) NOT NULL,
  logo_image_h VARCHAR(200) NOT NULL,
  logo_image_v VARCHAR(200) NOT NULL,
  logo_bg_color VARCHAR(7) NOT NULL
);

INSERT INTO distros (id, name, description, description_source, homepage, tags, logo_image_h, logo_image_v, logo_bg_color)
VALUES
(
  'ab2811b3-7c9a-4053-bca6-6f61e9ea2a88',
  'Arch',
  'Arch Linux is a Linux distribution for computers based on x86-64 ' ||
  'architectures. Arch Linux is composed of nonfree and open-source ' ||
  'software, and supports community involvement. The design approach ' ||
  'of the development team follows the KISS principle ("keep it simple, ' ||
  'stupid") as the general guideline, and focuses on elegance, code ' ||
  'correctness, minimalism and simplicity, and expects the user to be ' ||
  'willing to make some effort to understand the system''s operation. A ' ||
  'package manager written specifically for Arch Linux, pacman, is used to ' ||
  'install, remove and update software packages. Arch Linux uses a rolling ' ||
  'release model, such that a regular system update is all that is needed ' ||
  'to obtain the latest Arch software; the installation images released by ' ||
  'the Arch team are simply up-to-date snapshots of the main system ' ||
  'components. Arch Linux has comprehensive documentation in the form of a ' ||
  'community wiki known as the ArchWiki.',
  'https://en.wikipedia.org/wiki/Arch_Linux',
  'https://www.archlinux.org/',
  'Workstation,Server,IoT,Cloud',
  'archlinux-logo-dark-scalable.svg',
  'archlinux-logo-dark-scalable.svg',
  '#ecf2f5'
),(
  '8d7e426c-2d8c-4105-982b-0539d96bf97f',
  'CentOS',
  'CentOS is a Linux distribution that provides a free, enterprise-class, ' ||
  'community-supported computing platform functionally compatible with its ' ||
  'upstream source, Red Hat Enterprise Linux (RHEL). In January 2014, ' ||
  'CentOS announced the official joining with Red Hat while staying ' ||
  'independent from RHEL, under a new CentOS governing board. The first ' ||
  'CentOS release in May 2004, numbered as CentOS version 2, was forked ' ||
  'from RHEL version 2.1AS. Since the release of version 7.0, CentOS ' ||
  'officially supports only the x86-64 architecture, while versions older ' ||
  'than 7.0-1406 also support IA-32 with Physical Address Extension (PAE). ' ||
  'As of December 2015, AltArch releases of CentOS 7 are available for the ' ||
  'IA-32 architecture, Power ISA, and for the ARMv7hl and AArch64 variants ' ||
  'of the ARM architecture.',
  'https://en.wikipedia.org/wiki/CentOS',
  'https://www.centos.org/',
  'Workstation,Server,IoT,Cloud',
  'centos-logo-light.svg',
  'centos-logo-light-vertical.svg',
  '#ffffff'
),(
  '8a3942c6-f97a-4385-bdc1-9e9a0f562010',
  'Debian',
  'Debian is a Unix-like operating system consisting entirely of free ' ||
  'software. Ian Murdock founded the Debian Project on August 16, 1993. ' ||
  'Debian 0.01 was released on September 15, 1993, and the first stable ' ||
  'version, 1.1, was released on June 17, 1996. The Debian Stable branch '||
  'is the most popular edition for personal computers and network servers, ' ||
  'and is used as the basis for many other Linux distributions. Debian is ' ||
  'one of the earliest operating systems based on the Linux kernel. The ' ||
  'project is coordinated over the Internet by a team of volunteers guided ' ||
  'by the Debian Project Leader and three foundational documents: the ' ||
  'Debian Social Contract, the Debian Constitution, and the Debian Free ' ||
  'Software Guidelines. New distributions are updated continually, and the ' ||
  'next candidate is released after a time-based freeze. Debian has been ' ||
  'developed openly and distributed freely according to the principles of ' ||
  'the GNU Project. Because of this, the Free Software Foundation sponsored '||
  'the project from November 1994 to November 1995. The popular Linux ' ||
  'operating system Ubuntu was also released based on Debian. When the ' ||
  'sponsorship ended, the Debian Project formed the nonprofit Software in ' ||
  'the Public Interest to continue financially supporting development.',
  'https://en.wikipedia.org/wiki/Debian',
  'https://www.debian.org/',
  'Workstation,Server,IoT,Cloud',
  'debian-logo.svg',
  'debian-logo.svg',
  '#ffffff'
),(
  '85cab214-b9fc-44c3-9595-c4ba49ad43d9',
  'elementary',
  'elementary OS is a Linux distribution based on Ubuntu. It is the ' ||
  'flagship distribution to showcase the Pantheon desktop environment. The ' ||
  'distribution promotes itself as a “fast, open, and privacy-respecting” ' ||
  'replacement to macOS and Windows. It focuses mainly on non-technical ' ||
  'users, and has a pay-what-you-want model. The OS is developed by ' ||
  'elementary, Inc.',
  'https://en.wikipedia.org/wiki/Elementary_OS',
  'https://elementary.io/',
  'Workstation,Server,IoT,Cloud',
  'elementary-logotype-black.svg',
  'elementary-logomark-black.svg',
  '#ffffff'
),(
  'c52303c3-ccd6-4661-85bb-0fe02c79d402',
  'Fedora',
  'Fedora is a Linux distribution developed by the community-supported ' ||
  'Fedora Project and sponsored by Red Hat. Fedora contains software ' ||
  'distributed under various free and open-source licenses and aims to be ' ||
  'on the leading edge of such technologies. Fedora is the upstream source ' ||
  'of the commercial Red Hat Enterprise Linux distribution. Since the ' ||
  'release of Fedora 21, three different editions are currently available: ' ||
  'Workstation, focused on the personal computer, Server for servers, and ' ||
  'Atomic focused on cloud computing. As of February 2016, Fedora has an ' ||
  'estimated 1.2 million users, including Linus Torvalds, creator of the ' ||
  'Linux kernel.',
  'https://en.wikipedia.org/wiki/Fedora_(operating_system)',
  'https://getfedora.org/',
  'Workstation,Server,IoT,Cloud',
  'Fedora_logo_and_wordmark.svg',
  'Fedora_logo.svg',
  '#ffffff'
),(
  'f7944652-8ee4-474b-a060-f03f815f54ec',
  'Gentoo',
  'Gentoo Linux is a Linux distribution built using the Portage package ' ||
  'management system. Unlike a binary software distribution, the source ' ||
  'code is compiled locally according to the user''s preferences and is ' ||
  'often optimized for the specific type of computer. Precompiled binaries ' ||
  'are available for some larger packages or those with no available ' ||
  'source code. Gentoo Linux was named after the fast-swimming gentoo ' ||
  'penguin. The name was chosen to reflect the potential speed ' ||
  'improvements of machine-specific optimization, which is a major feature ' ||
  'of Gentoo. Gentoo package management is designed to be modular, ' ||
  'portable, easy to maintain, and flexible. Gentoo describes itself as a ' ||
  'meta-distribution because of its adaptability, in that the majority of ' ||
  'users have configurations and sets of installed programs which are ' ||
  'unique to the system and the applications they use.',
  'https://en.wikipedia.org/wiki/Gentoo_Linux',
  'https://www.gentoo.org/',
  'Workstation,Server,IoT,Cloud',
  'gentoo-horizontal.svg',
  'gentoo-logo.svg',
  '#ffffff'
),(
  'bf6d2cd4-c08c-4564-9cbd-534a9b2a96d2',
  'Kali',
  'Kali Linux is a Debian-derived Linux distribution designed for digital ' ||
  'forensics and penetration testing. It is maintained and funded by ' ||
  'Offensive Security Ltd. It was developed by Mati Aharoni and Devon ' ||
  'Kearns of Offensive Security through the rewrite of BackTrack, their ' ||
  'previous information security testing Linux distribution based on ' ||
  'Knoppix. The third core developer, Raphaël Hertzog, joined them as a ' ||
  'Debian expert.',
  'https://en.wikipedia.org/wiki/Kali_Linux',
  'https://www.kali.org/',
  'Workstation,Server,IoT,Cloud',
  'Kali_Linux_2.0_wordmark.svg',
  'Kali_Linux_2.0_wordmark.svg',
  '#ffffff'
),(
  'ed3a0238-a644-43f2-9856-3b58d90840c6',
  'KDE Neon',
  'KDE neon is a set of software repositories for Ubuntu long-term support ' ||
  '(LTS) releases with latest 64-bit version of KDE desktop and ' ||
  'applications. It is also the name given to a Ubuntu LTS-based Linux ' ||
  'distribution that utilizes said repositories. It aims to provide the ' ||
  'users with rapidly updated Qt and KDE software, while updating the rest ' ||
  'of the OS components from the Ubuntu repositories at the normal pace. ' ||
  'It comes in user and developer editions. The KDE neon Linux ' ||
  'distribution is focused on the development of KDE. The emphasis is on ' ||
  'bleeding edge software packages sourced directly from KDE and offers ' ||
  'programmers early access to new features, but potentially at the cost ' ||
  'of greater susceptibility to software bugs. KDE neon announced in ' ||
  'January 2017 that the distribution switching its installer from ' ||
  'Ubiquity to Calamares due to Ubiquity "not having some features". In ' ||
  'February 2018, KDE neon developers removed the LTS Editions from the ' ||
  'downloads page, but kept these editions in the download mirrors because ' ||
  'of "lots of people asking which edition to use and what the difference ' ||
  'is." In May 2018, KDE started changing KDE neon from being based on ' ||
  'Ubuntu 16.04 to Ubuntu 18.04. KDE neon preview images, based on Ubuntu ' ||
  '18.04, became available in August 2018.',
  'https://en.wikipedia.org/wiki/KDE_neon',
  'https://neon.kde.org/',
  'Workstation,Server,IoT,Cloud',
  'kde-neon-horizontal.png',
  'kde-neon-vertical.png',
  '#ffffff'
),(
  '1b512678-038d-456c-96b2-40dc7125ab6b',
  'Linux Mint',
  'Linux Mint is a community-driven Linux distribution based on Debian and ' ||
  'Ubuntu that strives to be a "modern, elegant and comfortable operating ' ||
  'system which is both powerful and easy to use." Linux Mint provides ' ||
  'full out-of-the-box multimedia support by including some proprietary ' ||
  'software and comes bundled with a variety of free and open-source ' ||
  'applications. The project was conceived by Clément Lefèbvre and is ' ||
  'being actively developed by the Linux Mint Team and community.',
  'https://en.wikipedia.org/wiki/Linux_Mint',
  'https://linuxmint.com/',
  'Workstation,Server,IoT,Cloud',
  'Linux_Mint_Official_Logo.svg',
  'linux-mint-logo.png',
  '#ffffff'
),(
  '2dd45d92-3434-4163-86ab-63037d080540',
  'Manjaro',
  'Manjaro is an open-source Linux distribution based on the Arch Linux ' ||
  'operating system. Manjaro has a focus on user friendliness and ' ||
  'accessibility, and the system itself is designed to work fully ' ||
  '"straight out of the box" with its variety of pre-installed software. ' ||
  'It features a rolling release update model and uses pacman as its ' ||
  'package manager.',
  'https://en.wikipedia.org/wiki/Manjaro',
  'https://manjaro.org/',
  'Workstation,Server,IoT,Cloud',
  'manjaro_logo_text.png',
  'manjaro_logo.svg',
  '#ffffff'
),(
  '3bdcf61d-986e-492d-a0d0-a83384ad3397',
  'openSuse',
  'openSUSE, formerly SUSE Linux and SuSE Linux Professional, is a Linux ' ||
  'distribution sponsored by SUSE Linux GmbH and other companies. It is ' ||
  'widely used throughout the world. The focus of its development is ' ||
  'creating usable open-source tools for software developers and system ' ||
  'administrators, while providing a user-friendly desktop and ' ||
  'feature-rich server environment. The initial release of the community ' ||
  'project was a beta version of SUSE Linux 10.0. The current stable ' ||
  'release is openSUSE Leap 15.0. The community project offers a rolling ' ||
  'release version called openSUSE Tumbleweed, which is continuously ' ||
  'updated with tested, stable packages. This is based on the rolling ' ||
  'development code base called "Factory". Other tools and applications ' ||
  'associated with the openSUSE project are YaST, Open Build Service, ' ||
  'openQA, Snapper, Machinery, Portus and Kiwi. Novell created openSUSE ' ||
  'after purchasing SuSE Linux AG for US$210 million on 4 November 2003. ' ||
  'The Attachmate Group acquired Novell and split Novell and SUSE into two ' ||
  'autonomous subsidiary companies. After The Attachmate Group merged with ' ||
  'Micro Focus in November 2014, SUSE became its own business unit. On 4 ' ||
  'July 2018, EQT Partners purchased SUSE for 2.5 billion USD.',
  'https://en.wikipedia.org/wiki/OpenSUSE',
  'https://www.opensuse.org/',
  'Workstation,Server,IoT,Cloud',
  'open-suse-logo.png',
  'open-suse-logo.png',
  '#35b9ab'
),(
  '02249331-8061-4544-9840-2cfa972450be',
  'Red Hat Enterprise Linux',
  'Red Hat Enterprise Linux is a Linux distribution developed by Red Hat ' ||
  'and targeted toward the commercial market. Red Hat Enterprise Linux is ' ||
  'released in server versions for x86-64, Power ISA, ARM64, and IBM Z, ' ||
  'and a desktop version for x86-64. All of Red Hat''s official support ' ||
  'and training, together with the Red Hat Certification Program, focuses ' ||
  'on the Red Hat Enterprise Linux platform. Red Hat Enterprise Linux is ' ||
  'often abbreviated to RHEL. The first version of Red Hat Enterprise ' ||
  'Linux to bear the name originally came onto the market as "Red Hat ' ||
  'Linux Advanced Server". In 2003 Red Hat rebranded Red Hat Linux ' ||
  'Advanced Server to "Red Hat Enterprise Linux AS", and added two more ' ||
  'variants, Red Hat Enterprise Linux ES and Red Hat Enterprise Linux WS. ' ||
  'Red Hat uses strict trademark rules to restrict free re-distribution of ' ||
  'their officially supported versions of Red Hat Enterprise Linux, but ' ||
  'still freely provides its source code. Third-party derivatives can be ' ||
  'built and redistributed by stripping away non-free components like Red ' ||
  'Hat''s trademarks. Examples include community-supported distributions ' ||
  'like CentOS and Scientific Linux, and commercial forks like Oracle ' ||
  'Linux. In October 2018, Red Hat declared that KDE Plasma was no longer ' ||
  'supported in future updates of Red Hat Enterprise Linux. The ' ||
  'announcement came shortly after the announcement of the business ' ||
  'acquisition of Red Hat by IBM for close to $34 billion USD.',
  'https://en.wikipedia.org/wiki/Red_Hat_Enterprise_Linux',
  'https://www.redhat.com/',
  'Workstation,Server,IoT,Cloud',
  'redhat-logo-h.svg',
  'redhat-logo-v.svg',
  '#ededed'
),(
  'df44b983-3e75-4792-9330-55d667f2eeb2',
  'Slackware',
  'Slackware is a Linux distribution created by Patrick Volkerding in ' ||
  '1993. Originally based on Softlanding Linux System, Slackware has been ' ||
  'the basis for many other Linux distributions, most notably the first ' ||
  'versions of SUSE Linux distributions, and is the oldest distribution ' ||
  'that is still maintained. Slackware aims for design stability and ' ||
  'simplicity and to be the most "Unix-like" Linux distribution. It makes ' ||
  'as few modifications as possible to software packages from upstream ' ||
  'and tries not to anticipate use cases or preclude user decisions. In ' ||
  'contrast to most modern Linux distributions, Slackware provides no ' ||
  'graphical installation procedure and no automatic dependency resolution ' ||
  'of software packages. It uses plain text files and only a small set of ' ||
  'shell scripts for configuration and administration. Without further ' ||
  'modification it boots into a command-line interface environment. ' ||
  'Because of its many conservative and simplistic features, Slackware is ' ||
  'often considered to be most suitable for advanced and technically ' ||
  'inclined Linux users. Slackware is available for the IA-32 and x86_64 ' ||
  'architectures, with a port to the ARM architecture. While Slackware is ' ||
  'mostly free and open source software, it does not have a formal bug ' ||
  'tracking facility or public code repository, with releases periodically ' ||
  'announced by Volkerding. There is no formal membership procedure for ' ||
  'developers and Volkerding is the primary contributor to releases.',
  'https://en.wikipedia.org/wiki/Slackware',
  'http://www.slackware.com/',
  'Workstation,Server,IoT,Cloud',
  'slackware_logo_horizontal.png',
  'slackware_logo_vertical.png',
  '#ffffff'
),(
  'ca36c1fd-44db-452a-8321-20bfbf48acfa',
  'Solus',
  'Solus is an independent desktop operating system based on the Linux ' ||
  'kernel. It is offered as a curated rolling release model under the ' ||
  'slogan "Install Today. Updates Forever". Solus contains a wide variety ' ||
  'of desktop environments depending on release chosen, options include ' ||
  'Solus''s own Budgie Desktop, GNOME, MATE and soon KDE Plasma.',
  'https://en.wikipedia.org/wiki/Solus_(operating_system)',
  'https://getsol.us/',
  'Workstation,Server,IoT,Cloud',
  'Solus_With_Text.png',
  'Solus.svg',
  '#ffffff'
),(
  'c3e7f5bb-be6a-45e0-98dc-f5010969596b',
  'Ubuntu',
  'Ubuntu is a free and open-source Linux distribution based on Debian. ' ||
  'Ubuntu is officially released in three editions: Desktop, Server, and ' ||
  'Core (for internet of things devices and robots). Ubuntu is a popular ' ||
  'operating system for cloud computing, with support for OpenStack. ' ||
  'Ubuntu is released every six months, with long-term support (LTS) ' ||
  'releases every two years. The latest release is 19.04 ("Disco Dingo"), ' ||
  'and the most recent long-term support release is 18.04 LTS ("Bionic ' ||
  'Beaver"), which is supported until 2028. Ubuntu is developed by ' ||
  'Canonical and the community under a meritocratic governance model. ' ||
  'Canonical provides security updates and support for each Ubuntu ' ||
  'release, starting from the release date and until the release reaches ' ||
  'its designated end-of-life (EOL) date. Canonical generates revenue ' ||
  'through the sale of premium services related to Ubuntu. Ubuntu is named ' ||
  'after the African philosophy of ubuntu, which Canonical translates as ' ||
  '"humanity to others" or "I am what I am because of who we all are".',
  'https://en.wikipedia.org/wiki/Ubuntu',
  'https://www.ubuntu.com/',
  'Workstation,Server,IoT,Cloud',
  'ubuntu_white-orange_hex.svg',
  'ubuntu_white-orange_hex.svg',
  '#e95420'
)
ON CONFLICT (name)
DO NOTHING;

--------------
-- Ratings
--------------
CREATE TABLE IF NOT EXISTS ratings (
  id UUID PRIMARY KEY,
  distro_id UUID REFERENCES distros(id),
  username VARCHAR(100),
  rate_overall DOUBLE PRECISION DEFAULT(1),
  rate_workstation DOUBLE PRECISION DEFAULT(1),
  rate_server DOUBLE PRECISION DEFAULT(1),
  rate_iot DOUBLE PRECISION DEFAULT(1),
  comment VARCHAR(10000),
  upvotes NUMERIC DEFAULT(0)
);
