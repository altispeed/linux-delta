**Name**:
_The project's preferred name._

**Description**:
_A brief description of the goals of the project and what they offer users. What differentiates them from other projects._

**Description Source**:
_URL of the above description, if it is taken from another source. (i.e, the project's homepage, wikipedia, distrowatch, etc...)_

**Homepage**:
_URL to the project's homepage._

**Horizontal Logo**:
_This is the image that shows up on the Linux Delta homepage. Generally contains a logo/symbol with the projects name to the right or left. File type preference: `.svg` -> medium sized `.png` -> large sized `.png`_

**Vertical Logo**:
_This is the image that shows up on the project's details page. Generally contains a logo/symbol with the projects name above or below. Can sometimes just be the logo/symbol. File type preference: `.svg` -> medium sized `.png` -> large sized `.png`_

**Logo BG Color (optional)**:
_The hex color code of the preferred background color for the logos. Example: `#ff0000`_  


/label ~"New Distro"

/cc @Kpovoc  

