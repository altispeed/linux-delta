module gitlab.com/altispeed/linux-delta

go 1.14

require (
	github.com/go-pg/pg v8.0.6+incompatible
	github.com/gorilla/mux v1.7.4
	github.com/gorilla/securecookie v1.1.1
	github.com/jinzhu/inflection v1.0.0 // indirect
	github.com/satori/go.uuid v1.2.1-0.20181028125025-b2ce2384e17b
	golang.org/x/crypto v0.0.0-20200406173513-056763e48d71
	mellium.im/sasl v0.2.1 // indirect
)
