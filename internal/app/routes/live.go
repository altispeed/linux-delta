/**
 * Copyright (C) 2020 KhronoSync Digital Solutions LLC
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>
 */
package routes

import (
	"github.com/gorilla/mux"
	"net/http"
)

const PathLive = "/live"
const PathAskNoahShow = PathLive + "/ans"
const PathSouthEastLinuxFest = PathLive + "/self"

type LivePage struct {
	Session *SessionModel
	Title string
	TitleImage string
	DefaultSource string
	AudioSource string
	AudioType string
	VideoSource string
	VideoPoster string
	IrcChatSource string
	IrcChatNote string
}

func initializeLiveRoutes(router *mux.Router) {
	router.HandleFunc(PathLive, newAnsHandler).Methods(http.MethodGet)
	router.HandleFunc(PathAskNoahShow, ansHandler).Methods(http.MethodGet)
	router.HandleFunc(PathSouthEastLinuxFest, selfHandler).Methods(http.MethodGet)
}

func ansHandler(w http.ResponseWriter, r *http.Request) {
	p := &LivePage{
		Session: getSession(r),
		Title: "Ask Noah",
		TitleImage: "asknoah-horizontal-logo.png",
		DefaultSource: "audio",
		AudioSource: "http://stream.radiojar.com/gy6eza9n6p5tv.mp3",
		AudioType: "audio/mpeg",
		VideoSource: "https://altispeed-hls.secdn.net/altispeed-live/play/mdmstream/playlist.m3u8",
		VideoPoster: "/static/images/asknoah-logo-poster.png",
		IrcChatSource: "https://kiwiirc.com/client/chat.freenode.net/?nick=ans-live|?&theme=cli#asknoahshow",
		IrcChatNote: "",
	}

	renderLiveTemplate(w, p)
}

func newAnsHandler(w http.ResponseWriter, r *http.Request) {
	p := &LivePage{
		Session: getSession(r),
		Title: "Ask Noah",
		TitleImage: "asknoah-horizontal-logo.png",
		DefaultSource: "audio",
		AudioSource: "http://stream.radiojar.com/gy6eza9n6p5tv.mp3",
		AudioType: "audio/mpeg",
		VideoSource: "https://altispeed-hls.secdn.net/altispeed-live/play/mdmstream/playlist.m3u8",
		VideoPoster: "/static/images/asknoah-logo-poster.png",
		IrcChatSource: "https://kiwiirc.com/client/chat.freenode.net/?nick=ans-live|?&theme=cli#asknoahshow",
		IrcChatNote: "",
	}

	renderNewLiveTemplate(w, p)
}

func selfHandler(w http.ResponseWriter, r *http.Request) {
	p := &LivePage{
		Session: getSession(r),
		Title: "SouthEast LinuxFest",
		TitleImage: "self-logo-full.png",
		DefaultSource: "video",
		AudioSource: "http://stream.radiojar.com/gy6eza9n6p5tv.mp3",
		AudioType: "audio/mpeg",
		VideoSource: "https://altispeed-hls.secdn.net/altispeed-live/play/mdmstream/playlist.m3u8",
		VideoPoster: "/static/images/self-logo-poster.png",
		IrcChatSource: "https://kiwiirc.com/client/chat.freenode.net/?theme=cli#southeastlinuxfest",
		IrcChatNote: "NOTE: #southeastlinuxfest requires a registered Nickname and Password in order " +
			"to immediately connect on login.",
	}

	renderLiveTemplate(w, p)
}

func renderLiveTemplate(w http.ResponseWriter, page *LivePage) {
	err := templates.ExecuteTemplate(w, "live_page.html", page)
	if err != nil {
		http.Error(w, err.Error(), http.StatusInternalServerError)
	}
}

func renderNewLiveTemplate(w http.ResponseWriter, page *LivePage) {
	err := templates.ExecuteTemplate(w, "new_live_page.html", page)
	if err != nil {
		http.Error(w, err.Error(), http.StatusInternalServerError)
	}
}