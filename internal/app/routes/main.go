package routes

/**
 * Copyright (C) 2019 KhronoSync Digital Solutions LLC
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>
 */

import (
  "github.com/gorilla/mux"
  "gitlab.com/altispeed/linux-delta/internal/app/conf"
  "net/http"
)

func Init(adminList map[string]string, recaptchaConf conf.RecaptchaConfig) {
  templates = initializeTemplates("resources/templates/")
  cookieHandler = initializeCookieHandler()
  router := mux.NewRouter()
  admins = adminList

  initializeHomeRoutes(router)
  initializeReviewHomeRoutes(router)
  initializeDistroRoutes(router, recaptchaConf)
  initializeLiveRoutes(router)
  initializeLoginRoutes(router)

  http.Handle("/", router)

  // Static files
  http.Handle(
    "/static/",
    http.StripPrefix(
      "/static/",
      http.FileServer(http.Dir("resources/static"))))
  http.Handle(
    "/.well-known/",
    http.StripPrefix(
      "/.well-known/",
      http.FileServer(http.Dir("resources/well-known"))))
}