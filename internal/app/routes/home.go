package routes

/**
 * Copyright (C) 2020 KhronoSync Digital Solutions LLC
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>
 */

import (
	"github.com/gorilla/mux"
	"net/http"
)

const PathHome = "/"
const PathFavicon = "/favicon.ico"

type HomePage struct {
	Session *SessionModel
}

func initializeHomeRoutes(router *mux.Router) {
	router.HandleFunc(PathHome, homeHandler)
	router.HandleFunc(PathFavicon, faviconHandler)
}

func faviconHandler(w http.ResponseWriter, r *http.Request) {
	http.ServeFile(w, r, "./resources/static/images/favicon.ico")
}

func homeHandler(w http.ResponseWriter, r *http.Request) {
	p := &LivePage{
		Session:       getSession(r),
		Title:         "Ask Noah",
		TitleImage:    "asknoah-horizontal-logo.png",
		DefaultSource: "audio",
		AudioSource:   "http://stream.radiojar.com/gy6eza9n6p5tv.mp3",
		AudioType:     "audio/mpeg",
		VideoSource:   "https://altispeed-hls.secdn.net/altispeed-live/play/mdmstream/playlist.m3u8",
		VideoPoster:   "/static/images/asknoah-logo-poster.png",
		IrcChatSource: "https://kiwiirc.com/client/chat.freenode.net/?nick=ans-live|?&theme=cli#asknoahshow",
		IrcChatNote:   "",
	}

	renderHomeTemplate(w, p)
}

func renderHomeTemplate(w http.ResponseWriter, page *LivePage) {
	err := templates.ExecuteTemplate(w, "new_live_page.html", page)
	if err != nil {
		http.Error(w, err.Error(), http.StatusInternalServerError)
	}
}

//func renderHomeTemplate(w http.ResponseWriter, page *HomePage) {
//	err := templates.ExecuteTemplate(w, "home_page.html", page)
//	if err != nil {
//		http.Error(w, err.Error(), http.StatusInternalServerError)
//	}
//}
