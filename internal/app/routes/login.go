/**
 * Copyright (C) 2020 KhronoSync Digital Solutions LLC
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>
 */
package routes

import (
	"github.com/gorilla/mux"
	"golang.org/x/crypto/bcrypt"
	"net/http"
	"strings"
)

const PathLogin = "/login"
const PathLogout = "/logout"

type LoginPage struct {
	Session *SessionModel
	ErrorMsg string
}

func initializeLoginRoutes(router *mux.Router) {
	router.HandleFunc(PathLogin, loginPageHandler).Methods(http.MethodGet)
	router.HandleFunc(PathLogin, loginFormHandler).Methods(http.MethodPost)
	router.HandleFunc(PathLogout, logoutFormHandler).Methods(http.MethodPost)
}

func loginPageHandler(w http.ResponseWriter, r *http.Request) {
	p := &LoginPage{
		Session: getSession(r),
	}

	renderLoginTemplate(w, p)
}

func loginFormHandler(w http.ResponseWriter, r *http.Request) {
	name :=  strings.TrimSpace(r.FormValue("name"))
	unhashedPass := strings.TrimSpace(r.FormValue("pass"))
	hashedPass := admins[name]

	if checkPasswordHash(unhashedPass, hashedPass) {
		setSession(name, w)
		http.Redirect(w, r, "/", http.StatusFound)
	} else {
		p := &LoginPage{
			ErrorMsg: "Unrecognized username/password combination",
			Session: getSession(r),
		}

		renderLoginTemplate(w, p)
	}
}

func logoutFormHandler(w http.ResponseWriter, r *http.Request) {
	clearSession(w)
	http.Redirect(w, r, "/", http.StatusFound)
}

func renderLoginTemplate(w http.ResponseWriter, page *LoginPage) {
	err := templates.ExecuteTemplate(w, "login_page.html", page)
	if err != nil {
		http.Error(w, err.Error(), http.StatusInternalServerError)
	}
}

func checkPasswordHash(password, hash string) bool {
	err := bcrypt.CompareHashAndPassword([]byte(hash), []byte(password))
	return err == nil
}