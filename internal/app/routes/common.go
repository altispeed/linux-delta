package routes
/**
 * Copyright (C) 2020 KhronoSync Digital Solutions LLC
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>
 */

import (
	"fmt"
	sc "github.com/gorilla/securecookie"
	"html/template"
	"net/http"
	"strings"
)

const EmptyStar = `<i class="star-empty"></i>`
const HalfStar = `<i class="star-half"></i>`
const FullStar = `<i class="star-full"></i>`
const StarRating0 = template.HTML(
	EmptyStar + EmptyStar + EmptyStar + EmptyStar + EmptyStar)
const StarRating0Half = template.HTML(
	HalfStar + EmptyStar + EmptyStar + EmptyStar + EmptyStar)
const StarRating1 = template.HTML(
	FullStar + EmptyStar + EmptyStar + EmptyStar + EmptyStar)
const StarRating1Half = template.HTML(
	FullStar + HalfStar + EmptyStar + EmptyStar + EmptyStar)
const StarRating2 = template.HTML(
	FullStar + FullStar + EmptyStar + EmptyStar + EmptyStar)
const StarRating2Half = template.HTML(
	FullStar + FullStar + HalfStar + EmptyStar + EmptyStar)
const StarRating3 = template.HTML(
	FullStar + FullStar + FullStar + EmptyStar + EmptyStar)
const StarRating3Half = template.HTML(
	FullStar + FullStar + FullStar + HalfStar + EmptyStar)
const StarRating4 = template.HTML(
	FullStar + FullStar + FullStar + FullStar + EmptyStar)
const StarRating4Half = template.HTML(
	FullStar + FullStar + FullStar + FullStar + HalfStar)
const StarRating5 = template.HTML(
	FullStar + FullStar + FullStar + FullStar + FullStar)

type SessionModel struct {
	Id string
	Username string
}

var templates *template.Template
var cookieHandler *sc.SecureCookie
var admins map[string]string

func initializeTemplates(tmplRoot string) *template.Template {
	return template.Must(
		template.
			New("main").
			Funcs(
				template.FuncMap {
					"getStarRating": getGetStarRatingFunc(),
					"starCtrl": getStarCtrlFunc(),
					"twoDecFloat": get2DecimalFloatFunc(),
				}).
			ParseFiles(
				tmplRoot + "header.html",
				tmplRoot + "footer.html",
				tmplRoot + "home_page.html",
				tmplRoot + "reviews_home_page.html",
				tmplRoot + "distro_page.html",
				tmplRoot + "login_page.html",
				tmplRoot + "live_page.html",
				tmplRoot + "new_live_page.html",
			))
}

func initializeCookieHandler() *sc.SecureCookie {
	// TODO: May need to read in hashKey and blockKey from config in order to
	// persist sessions across restarts
	hashKey := sc.GenerateRandomKey(64)
	blockKey := sc.GenerateRandomKey(32)

	return sc.New(hashKey, blockKey)
}

func getGetStarRatingFunc() func(rating float64) template.HTML {
	return func(rating float64) template.HTML {
		if rating < 0.5 { return StarRating0 }
		if rating < 1.0 { return StarRating0Half }
		if rating < 1.5 { return StarRating1 }
		if rating < 2.0 { return StarRating1Half }
		if rating < 2.5 { return StarRating2 }
		if rating < 3.0 { return StarRating2Half }
		if rating < 3.5 { return StarRating3 }
		if rating < 4.0 { return StarRating3Half }
		if rating < 4.5 { return StarRating4 }
		if rating < 5.0 { return StarRating4Half }
		return StarRating5
	}
}

func getStarCtrlFunc() func(ctrlName string) template.HTML {
	return func(ctrlName string) template.HTML {
		s5 := starInputAndLabel(ctrlName, "5")
		s4 := starInputAndLabel(ctrlName, "4")
		s3 := starInputAndLabel(ctrlName, "3")
		s2 := starInputAndLabel(ctrlName, "2")
		s1 := starInputAndLabel(ctrlName, "1")
		x := clearStarInputAndLabel(ctrlName)

		return template.HTML(
			`<div class="stars">`+s5+s4+s3+s2+s1+x+`</div>`,
		)
	}
}

func get2DecimalFloatFunc() func(f float64) template.HTML {
	return func(f float64) template.HTML {
		return template.HTML(fmt.Sprintf("%.2f", f))
	}
}

func getSession(r *http.Request) *SessionModel {
	sess := &SessionModel{
		Id: "",
		Username: "",
	}

	if cookie, err := r.Cookie("username"); err == nil {
		cookieValue := make(map[string]string)
		if err = cookieHandler.Decode("username", cookie.Value, &cookieValue); err == nil {
			sess.Id = "00000000-0000-0000-0000-000000000000"
			sess.Username = cookieValue["username"]
		}
	}

	return sess
}

func setSession(username string, w http.ResponseWriter) {
	value := map[string]string {
		"username": username,
	}
	if encoded, err := cookieHandler.Encode("username", value); err == nil {
		cookie := &http.Cookie{
			Name: "username",
			Value: encoded,
			Path: "/",
			MaxAge: 3600,
		}
		http.SetCookie(w, cookie)
	}
}

func starInputAndLabel(name string, num string) string {
	cn := `"`+`star star-`+num+`" `
	id := `"`+name+`-star-`+num+`" `
	vl := `"`+num+`" `
	nm := `"`+name+`" `

	sb := strings.Builder{}
	sb.WriteString(`<input class=`)
	sb.WriteString(cn)
	sb.WriteString(`id=`)
	sb.WriteString(id)
	sb.WriteString(`type="radio" value=`)
	sb.WriteString(vl)
	sb.WriteString(`name=`)
	sb.WriteString(nm)

	if num == "0" {
		sb.WriteString(`checked `)
	}

	sb.WriteString(`/><label class=`)
	sb.WriteString(cn)
	sb.WriteString(`for=`)
	sb.WriteString(id)
	sb.WriteString(`></label>`)
	return sb.String()
}

func clearStarInputAndLabel(name string) string {
	cn := `"clear-star" `
	id := `"`+name+`-star-0" `
	vl := `"0" `
	nm := `"`+name+`" `

	sb := strings.Builder{}
	sb.WriteString(`<input class=`)
	sb.WriteString(cn)
	sb.WriteString(`id=`)
	sb.WriteString(id)
	sb.WriteString(`type="radio" value=`)
	sb.WriteString(vl)
	sb.WriteString(`name=`)
	sb.WriteString(nm)
	sb.WriteString(`checked `)
	sb.WriteString(`/><label class=`)
	sb.WriteString(cn)
	sb.WriteString(`title="Clear Rating" `)
	sb.WriteString(`for=`)
	sb.WriteString(id)
	sb.WriteString(`></label>`)

	return sb.String()
}

func clearSession(w http.ResponseWriter) {
	cookie := &http.Cookie{
		Name: "username",
		Value: "",
		Path: "/",
		MaxAge: -1,
	}
	http.SetCookie(w, cookie)
}