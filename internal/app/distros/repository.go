/**
 * Copyright (C) 2019 KhronoSync Digital Solutions LLC
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>
 */
package distros

import (
	"github.com/go-pg/pg"
	uuid "github.com/satori/go.uuid"
	"log"
)

type DistroRepository struct {
	db *pg.DB
}

func (dr *DistroRepository) Create(dm *DistroModel) (*DistroModel, error) {
	_, err := dr.db.Model(dm).Returning("*").Insert()
	if err != nil {
		log.Printf("Error while creating new Distro. Reason: %v\n", err)
		return nil, err
	}

	log.Printf("Created new Distro '%s'\n", dm.Name)
	return dm, nil
}

func (dr *DistroRepository) Read() ([]DistroModel, error) {
	var distros []DistroModel
	err := dr.db.Model(&distros).Order("name ASC").Select()
	if err != nil {
		log.Printf("Error reading all Distros. Reason: %v\n", err)
		return nil, err
	}

	log.Printf("Successfully retrieved distros\n")
	return distros, nil
}

func (dr *DistroRepository) ReadById(id uuid.UUID) (*DistroModel, error) {
	distro := &DistroModel{Id: id}
	err := dr.db.Select(distro)
	if err != nil {
		log.Printf(
			"Error reading Distro where ID=%v. Reason: %v\n",
			id, err,
		)
		return nil, err
	}

	return distro, nil
}

func (dr *DistroRepository) ReadByName(name string) (*DistroModel, error) {
	distro := new(DistroModel)
	err := dr.db.Model(distro).Where("name = ?", name).Select()
	if err != nil {
		log.Printf(
			"Error reading Distro where name='%s'. Reason: %v\n",
			name, err,
		)
		return nil, err
	}

	return distro, nil
}

//func CreateDistroTable(db *pg.DB) error {
//	opts := &orm.CreateTableOptions{
//		IfNotExists: true,
//	}
//	createErr := db.CreateTable(&DistroModel{}, opts)
//	if createErr != nil {
//		log.Printf(
//			"Error while creating table distros, Reason: %v\n",
//			createErr,
//		)
//		return createErr
//	}
//
//	log.Printf("Table `distros` created successfully.\n")
//	return nil
//}