/**
 * Copyright (C) 2019 KhronoSync Digital Solutions LLC
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>
 */
package main

import (
  "gitlab.com/altispeed/linux-delta/internal/app/conf"
  "gitlab.com/altispeed/linux-delta/internal/app/db"
  "gitlab.com/altispeed/linux-delta/internal/app/distros"
  "gitlab.com/altispeed/linux-delta/internal/app/ratings"
  "gitlab.com/altispeed/linux-delta/internal/app/routes"
  "log"
  "net/http"
)

const configPath = "./config/main-conf.json"

func main() {
  config := conf.LoadConfig(configPath)
  pgDb := db.Connect(&config.DB)
  distros.Init(pgDb)
  ratings.Init(pgDb)
  routes.Init(config.Admins, config.Recaptcha)

  err := http.ListenAndServe(":8080", nil)

  db.Close(pgDb)

  if err != nil {
    log.Fatal(err)
  }
}
