# [Linux Delta](https://linuxdelta.com/)

A web application meant to provide a system for users to rate and comment on 
their favorite Linux distributions. Created in partnership with 
[Altispeed Technologies](https://www.altispeed.com/) and the
[Ask Noah Show](https://asknoahshow.com) podcast. You can view the site at 
[linuxdelta.com](https://linuxdelta.com).

## Requirements
Go v1.14
Docker or PostgreSQL

## Quick Setup (Docker)
- Install [Go](https://golang.org/doc/install)  
- Install [Docker](https://docs.docker.com/get-docker/)  
- Install postgresql-client:  
  - *Ubuntu:*  
    ```bash
    sudo apt install postgresql-client
    ```  
  - *Arch/Manjaro:*
    ```bash
    sudo pacman -Syu postgresql-client
    ```  
- Clone the repository
  ```bash
  git clone git@gitlab.com:altispeed/linux-delta.git
  ```
- Go to the linux-delta directory and download the go modules
  ```bash
  cd ./linux-delta

  go mod download
  ```
- Copy the example configuration file to main-conf.json  
  ```bash
  cp ./config/example-conf.json ./config/main-conf.json
  ```
- Generate an admin account
  ```bash
  go run ./cmd/manage_admins
  ```
- Copy the data to `main-conf.json`
  ```json
  {
    "admins": {
      "Bobadmin": "$ha$hed$pass.wordHere.AbCD12345.ZxCvA9r8dKjkmdna"
    },
    ...
  }
  ```
- Set DB section in `main-conf.json`
  ```json
  "db": {
    "address": "localhost:5432",
    "username": "postgres",
    "password": "devpass"
  }
  ```
- Pull the official postgres docker image  
  ```bash
  docker pull postgres
  ```

- I like to have a separate directory for storing the postgres data per application, so 
  I make the following directory:
  ```bash
  mkdir -p ~/workspace/postgres/linux-delta
  ```
- Run the docker container. If you used a different directory than above, make sure it is
  reflected in the `-v` option below:
  ```bash
  docker run --rm --name pg-docker \
    -e POSTGRES_PASSWORD=devpass \
    -d \
    -p 5432:5432 \
    -v $HOME/workspace/postgres/linux-delta:/var/lib/postgresql/data \
    postgres
  ```
  - Stop the database with `docker kill pg-docker`  
- On the first build, run the `build_db` script to build the tables and populate the 
  distros.  
  ```bash
  ./build_db
  ```
- Run the server  
  ```bash
  go run ./cmd/linux-delta/
  ```